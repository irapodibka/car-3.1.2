import com.company.details.Engine;
import com.company.professions.Driver;
import com.company.vehicles.Car;
import com.company.vehicles.Lorry;
import com.company.vehicles.SportCar;


    public class CarDemo {
        public static void main(String[] args) {
            Driver bmwDriver = new Driver("Подібка І.Б.", 25,  5);
            Engine bmwEngine = new Engine("60", "BMW");
            Car car = new Car("BMW", "C", 5000, bmwDriver, bmwEngine);

            Driver lorryDriver = new Driver("Подібка С.І", 34,  10);
            Engine lorryEngine = new Engine("30", "LorryEngine");
            Lorry lorry = new Lorry("Грузовик", "D", 8000, lorryDriver, lorryEngine, 70);

            Driver sportDriver = new Driver("Подібка І.П.", 58,  30);
            Engine sportEngine = new Engine("80", "SportEngine");
            SportCar sportCar = new SportCar("SportCar", "C", 4000, sportDriver, sportEngine, 300);

            System.out.println(car);
            System.out.println(lorry);
            System.out.println(sportCar);
        }
    }
